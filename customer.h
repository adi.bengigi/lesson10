#pragma once
#include <set>

#include "Item.h"

using namespace std;

class Customer
{
public:
	Customer(string);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item); //add item to the set
	void removeItem(Item); //remove item from the set

	//get and set functions
	string getName() const;
	set<Item> getItems() const;

	void setName(string name);
	//	void setItems(set<Item> items);
private:
	string _name;
	set<Item> _items;
};
