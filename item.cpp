#include <iostream>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <cctype>

#include "Item.h"

using namespace std;

Item::Item(string name, string serialNumber, double unitPrice)
{
	this->_name = name;
	if (!isSerialNumberValid(serialNumber))
	{
		throw invalid_argument("invalid serial number");
	}
	this->_serialNumber = serialNumber;
	if (unitPrice <= 0)
	{
		throw out_of_range("the price is less or equal to 0.");
	}
	this->_unitPrice = unitPrice;
	this->_count = 1;
}
Item::~Item()
{

}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}
bool Item::operator<(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}
bool Item::operator>(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}
bool Item::operator==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}
string Item::getName() const
{
	return this->_name;
}
string Item::getSerialNumber() const
{
	return this->_serialNumber;
}
int Item::getCount() const
{
	return this->_count;
}

void Item::setUnitPrice(double unitPrice) const
{
	if (unitPrice <= 0)
	{
		throw out_of_range("the price is less or equal to 0.");
	}
	this->_unitPrice = unitPrice;
}
void Item::setName(string name) const
{
	this->_name = name;
}
void Item::setSerialNumber(string serialNumber)
{
	if (!isSerialNumberValid(serialNumber))
	{
		throw invalid_argument("invalid serial number");
	}
	this->_serialNumber = serialNumber;
}
void Item::setCount(int count) const
{
	if (count < 1)
	{
		throw out_of_range("the count is less than 1");
	}
	this->_count = count;
}

bool Item::isSerialNumberValid(string serialNumber)
{
	if (serialNumber.length() != 5)
	{
		return false;
	}
	for (int i = 0; i < 5; ++i)
	{
		if (!isdigit(serialNumber[i]))
		{
			return false;
		}
	}
	return true;
}