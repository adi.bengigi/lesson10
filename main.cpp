#include "Customer.h"
#include "Item.h"

#include <map>
#include <stdexcept>

using namespace std;

void printStartMenu();
void printItemList(Item* itemList, int count);
void printUpdateMenu();
Customer readNewCustomer(string name, Item* itemList, int count);
void updateCustomer(Customer& c, Item* itemList, int count);

int main()
{
	map<string, Customer> abcCustomers = map<string, Customer>();
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };



	bool exit = false;
	while (!exit)
	{
		printStartMenu();
		string name;
		Customer customer;
		int option;
		map<string, Customer>::iterator it;
		double maxPrice;
		cin >> option;
		switch (option)
		{
		case 1:
			cout << "please enter your name: ";
			cin >> name;
			if (abcCustomers.find(name) != abcCustomers.end())
			{
				cout << "the customer name is already exists." << endl;
				break;
			}
			customer = readNewCustomer(name, itemList, 10);
			abcCustomers[name] = customer;
			break;
		case 2:
			cout << "please enter your name: ";
			cin >> name;
			if (abcCustomers.find(name) == abcCustomers.end())
			{
				cout << "the customer name not found." << endl;
				break;
			}
			updateCustomer(abcCustomers[name], itemList, 10);
			break;
		case 3:
			maxPrice = -1;
			for (it = abcCustomers.begin(); it != abcCustomers.end(); ++it)
			{
				double price = it->second.totalSum();
				if (price > maxPrice)
				{
					name = it->second.getName();
					maxPrice = price;
				}
			}
			cout << "The top customer is " << name << endl;
			break;
		case 4:
			exit = true;
			break;
		}
	}

	system("pause");
	return 0;
}

void printStartMenu()
{
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1.\t" << "to sign as a new customer and buy items" << endl;
	cout << "2.\t" << "to update existing customer's items" << endl;
	cout << "3.\t" << "to print the name of the customer whose bill is the highest" << endl;
	cout << "4.\t" << "to exit" << endl;
}
void printItemList(Item* itemList, int count)
{
	cout << "The items you can buy are: " << endl;
	for (int i = 0; i < count; ++i)
	{
		cout << (i + 1) << ".\t" << itemList[i].getName() << " price: " << itemList[i].getUnitPrice() << endl;
	}
}
void printUpdateMenu()
{
	cout << "1.\t" << "Add items" << endl;
	cout << "2.\t" << "Remove items" << endl;
	cout << "3.\t" << "Back to menu" << endl;
}
Customer readNewCustomer(string name, Item* itemList, int count)
{
	Customer c = Customer(name);
	printItemList(itemList, count);
	bool exit = false;
	while (!exit)
	{
		cout << "please enter your choice (0 to exit): ";
		int choice;
		cin >> choice;
		if (choice == 0)
		{
			exit = true;
		}
		else if (choice >= 1 && choice <= count)
		{
			c.addItem(itemList[choice - 1]);
		}
		else
		{
			cout << "Invalid item." << endl;
		}
	}
	return c;
}
void updateCustomer(Customer& c, Item* itemList, int count)
{
	bool exit = false;
	while (!exit)
	{
		printUpdateMenu();
		int option;
		cin >> option;
		if (option == 3)
		{
			exit = true;
		}
		else if (option < 0 || option > 3)
		{
			cout << "invalid option." << endl;
		}
		else
		{
			int choice;
			printItemList(itemList, count);
			cout << "enter enter your choice: ";
			cin >> choice;
			if (choice >= 1 && choice <= count)
			{
				if (option == 1)
				{
					c.addItem(itemList[choice - 1]);
				}
				else
				{
					try
					{
						c.removeItem(itemList[choice - 1]);
					}
					catch (invalid_argument e)
					{
						cout << "cannot remove the item" << endl;
					}

				}

			}
			else
			{
				cout << "Invalid item." << endl;
			}
		}
	}
}
