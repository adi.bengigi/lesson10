#pragma once
#include <iostream>
#include <string>
#include <stdexcept> 
#include <algorithm>


using namespace std;

class Item
{
public:
	Item(string, string, double);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions
	double getUnitPrice() const;
	string getName() const;
	string getSerialNumber() const;
	int getCount() const;

	void setUnitPrice(double unitPrice) const;
	void setName(string name) const;
	void setSerialNumber(string serialNumber);
	void setCount(int count) const;

private:
	mutable string _name;
	string _serialNumber; //consists of 5 numbers
	mutable int _count; //default is 1, can never be less than 1!
	mutable double _unitPrice; //always bigger than 0!
	static bool isSerialNumberValid(string serialNumber);
};