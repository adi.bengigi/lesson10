#include <set>

#include "Item.h"
#include "Customer.h"

using namespace std;

Customer::Customer(string name)
{
	this->_name = name;
	this->_items = set<Item>();
}
Customer::Customer()
{
	this->_name = "";
	this->_items = set<Item>();
}
Customer::~Customer()
{
	this->_items = set<Item>();
}
double Customer::totalSum() const
{
	double sum = 0;
	set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		sum += it->totalPrice();
	}
	return sum;
}
void Customer::addItem(Item item)
{
	set<Item>::iterator it = this->_items.find(item);
	if (it == this->_items.end())
	{
		this->_items.insert(Item(item));
	}
	else
	{
		it->setCount(it->getCount() + 1);
	}
}
void Customer::removeItem(Item item)
{
	set<Item>::iterator it = this->_items.find(item);
	if (it == this->_items.end())
	{
		throw invalid_argument("the items is not exists.");
	}
	else
	{
		if (it->getCount() == 1)
		{
			this->_items.erase(it);
		}
		else
		{
			it->setCount(it->getCount() - 1);
		}
	}
}

//get and set functions
string Customer::getName() const
{
	return this->_name;
}
set<Item> Customer::getItems() const
{
	return this->_items;
}

void Customer::setName(string name)
{
	this->_name = name;
}
/*
void Customer::setItems(set<Item> items)
{
	this->_items = items;
}
*/